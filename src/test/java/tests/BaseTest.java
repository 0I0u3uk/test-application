package tests;

import ru.yandex.qatools.allure.annotations.Step;

public class BaseTest {

    @Step("�������� �������� �������� ��������: \"{0}\"")
    protected boolean checkOperand(String operand){
        try{
            Integer.parseInt(operand);
            return true;
        }
        catch (NumberFormatException e){
            return false;
        }
    }

    @Step("�������� �������� �������� �������� \"{0}\"")
    protected boolean checkOperation(String operation){
        return operation.equals("+") || operation.equals("-") || operation.equals("*") || operation.equals("/");

    }

    @Step("�������� �������� �������� ���������� \"{0}\"")
    protected boolean checkResult(String result){
        try{
            Double.parseDouble(result);
            return true;
        }
        catch (NumberFormatException e){
            return false;
        }
    }

    @Step("��������, ��� ��������� ��������� \"{0}{2}{1} = {3}\" �� ����� ������������� ����� ���������� ��������")
    protected double calculate(String opLeft, String opRight, String operation, String result){
        int left = Integer.parseInt(opLeft);
        int right = Integer.parseInt(opRight);

        if (operation.equals("+")) {
            return left + right;

        } else if (operation.equals("-")) {
            return left - right;
        } else if (operation.equals("*")) {
            return left * right;
        } else if (operation.equals("/")) {
            return left / right;
        } else {
            return Double.parseDouble(null);
        }
    }
}
