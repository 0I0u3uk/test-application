package tests;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import model.FormulaModel;
import model.FormulaModelMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Title;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(JUnitParamsRunner.class)
@Title("����� �� ������������ ������ � �����")
public class ArithmeticTest extends BaseTest {

    @Test
    @FileParameters(value = "file:src\\test\\resources\\test_file.csv", mapper = FormulaModelMapper.class)
    @Step("�������� ������")
    public void OperationTest(FormulaModel formula){

        assertTrue("�� ������ �������� ������ ��������, ��������� ����� �����, �� �����: "
                + formula.getLeftOperand()
                ,checkOperand(formula.getLeftOperand()));

        assertTrue("�� ������ �������� ������� ��������, ��������� ����� �����, �� �����: "
                + formula.getRightOperand()
                ,checkOperand(formula.getRightOperand()));

        assertTrue("�� ������ �������� ��������, ��������� +-*/, �� �����: "
                + formula.getOperation()
                ,checkOperation(formula.getOperation()));

        assertTrue("�� ������ �������� ���������, ��������� ����� �����, �� ������ "
                + formula.getResult()
                ,checkResult(formula.getResult()));

        assertThat("�������� ��������� �������� �� ��������� � ��� ��� � ����� "
                ,calculate(formula.getLeftOperand(),
                        formula.getRightOperand(),
                        formula.getOperation(),
                        formula.getResult()),
                equalTo(Double.parseDouble(formula.getResult())));
    }
}



