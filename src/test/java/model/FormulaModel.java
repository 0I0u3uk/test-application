package model;

public class FormulaModel {

    private String leftOperand;

    private String rightOperand;

    private String operation;

    private String result;

    public String getLeftOperand(){return leftOperand;}
    public void setLeftOperand(String operand) {leftOperand = operand; }

    public String getRightOperand(){return rightOperand;}
    public void setRightOperand(String operand) {rightOperand = operand; }

    public String getOperation(){return operation;}
    public void setOperation(String op) throws IllegalStateException { operation = op; }

    public String getResult(){return result;}
    public void setResult(String res) {result = res; }
}
