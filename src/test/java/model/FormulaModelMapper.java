package model;

import junitparams.mappers.DataMapper;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;

public class FormulaModelMapper implements DataMapper {

    public Object[] map(Reader reader) {

        Iterable<CSVRecord> records = null;
        try {
            records = CSVFormat.DEFAULT.parse(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<FormulaModel> formulas = new LinkedList<FormulaModel>();
        for(CSVRecord record : records) {
            FormulaModel formula = new FormulaModel();

            String[] recordString = record.get(0).replace(" ", "").split(";");
            int numberOfFields = 4;
            if(recordString.length != numberOfFields) {
                throw new IllegalArgumentException ("������ �������� CSV ����� ���������� �� ����������, � ������ ������ ���� "
                        + numberOfFields + " ��������");
            }

            formula.setLeftOperand(recordString[0]);
            formula.setRightOperand(recordString[1]);
            formula.setOperation(recordString[2]);
            formula.setResult(recordString[3]);

            formulas.add(formula);
        }

        return formulas.toArray();
    }
}
